import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

import { Menu } from "antd";

import Naslovna from "../Body/Naslovna";
import Galerija from "../Body/Galerija/Galerija";
import Paketi from "../Body/Paketi/Paketi";
import Kontakt from "../Body/Kontakt";

import logo from "../../Assets/mgabela.png";

import "./Grid.css";

class Grid extends Component {
  render() {
    return (
      <Router>
        <div className="grid-container">
          <div className="menu">
            <div
              className="treci"
              style={{
                height: "30vh",
                width: "20vw",
                background: "#1890ff",
                textAlign: "center",
                alignItems: "center"
              }}
            >
              <img
                src={logo}
                style={{
                  background: "#1890ff",
                  height: "50%",
                  width: "80%",
                  marginTop: "7.5vh",
                  marginLeft: "1vw"
                }}
                alt="logo"
              />
            </div>
            <Menu
              style={{ width: "20vw" }}
              defaultSelectedKeys={["1"]}
              defaultOpenKeys={["sub1"]}
              mode={"inline"}
              theme={"light"}
            >
              <Menu.Item key="1">
                <Link to="/">Naslovna</Link>
              </Menu.Item>
              <Menu.Item key="2">
                <Link to="/galerija">Galerija</Link>
              </Menu.Item>
              <Menu.Item key="3">
                <Link to="/paketi">Paketi</Link>
              </Menu.Item>
              <Menu.Item key="4">
                <Link to="/kontakt">Kontakt</Link>
              </Menu.Item>
            </Menu>
          </div>
          <Switch>
            <Route exact path="/" component={Naslovna} className="main" />
            <Route path="/galerija" component={Galerija} className="main" />
            <Route path="/paketi" component={Paketi} className="main" />
            <Route path="/kontakt" component={Kontakt} className="main" />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default Grid;
