import React from "react";
import logo from "../../../Assets/wedding-2.jpg";
import logo1 from "../../../Assets/wedding-3.jpg";
import "react-image-gallery/styles/css/image-gallery.css";

import ImageGallery from "react-image-gallery";

class Slider extends React.Component {
  render() {
    const images = [
      {
        original: logo
      },
      {
        original: logo1
      }
    ];

    return <ImageGallery items={images} />;
  }
}

export default Slider;
