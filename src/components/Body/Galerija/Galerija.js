import React, { Component } from "react";
import Slideshow from "./Slider";

class Galerija extends Component {
  render() {
    return (
      <div
        style={{
          heigth: "100vh",
          width: "80vw",
          padding: "20px"
        }}
      >
        <Slideshow />
      </div>
    );
  }
}

export default Galerija;
