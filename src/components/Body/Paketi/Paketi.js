import React, { Component } from "react";
import Slider1 from "./Slider1";
import Slider2 from "./Slider2";
import Slider3 from "./Slider3";
import Slider4 from "./Slider4";
import Slider5 from "./Slider5";
import Slider6 from "./Slider6";
import Slider7 from "./Slider7";
import Slider8 from "./Slider8";
import Slider9 from "./Slider9";

class Paketi extends Component {
  render() {
    return (
      <div
        style={{
          display: "flex !important",
          flexDirection: "column",
          justifyContent: "center",
          height: "50vh",
          width: "80vw"
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-around"
          }}
        >
          <Slider1 />
          <Slider2 />
        </div>

        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-around"
          }}
        >
          <Slider3 />
          <Slider4 />
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-around"
          }}
        >
          <Slider5 />
          <Slider6 />
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-around"
          }}
        >
          <Slider7 />
          <Slider8 />
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-around"
          }}
        >
          <Slider9 />
        </div>
      </div>
    );
  }
}

export default Paketi;
