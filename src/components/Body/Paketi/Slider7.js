import React from "react";
import { Zoom } from "react-slideshow-image";
import logo from "../../../Assets/wedding-2.jpg";
import logo1 from "../../../Assets/wedding-3.jpg";
import "./Slider.css";

const images = [logo, logo1];

const zoomOutProperties = {
  duration: 5000,
  transitionDuration: 500,
  infinite: true,
  indicators: true,
  scale: 0.4,
  arrows: true
};

const Slideshow = () => {
  return (
    <div className="slide-container">
      <h2 style={{ fontFamily: "cursive" }}>Books parents M</h2>
      <Zoom {...zoomOutProperties}>
        {images.map((each, index) => (
          <img key={index} className="slide_images" src={each} alt={index} />
        ))}
      </Zoom>
    </div>
  );
};

export default Slideshow;
