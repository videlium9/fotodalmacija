import React, { Component } from "react";
import gabelaPic from "../../Assets/wedding-3.jpg";
import { Button } from "antd";
import "./Naslovna.css";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import * as clickerActions from "../../redux/modules/clicker";

class Naslovna extends Component {
  // constructor(props) {
  //   super(props);
  // }

  updateClicks = () => {
    console.log("clientClicks", this.props.clientClicks);
  };

  componentDidMount() {
    this.props.updateOnClick(this.props.clientClicks);
  }
  render() {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-around",
          height: "100vh"
        }}
      >
        <div
          style={{
            width: "30vw",
            display: "flex",
            flexDirection: "column",
            justifyContent: "center"
          }}
        >
          <h1
            style={{
              marginBottom: "30px",
              textAlign: "center"
            }}
          >
            Miro Gabela
          </h1>
          <p
            style={{
              textAlign: "justify"
            }}
          >
            Siguran sam da nisam rođen da budem fotograf. Netko "tamo gore" je
            za mene namjenio sasvim drugu ulogu. Mislim da sam trebao biti
            lokalni svirač ili pjevač s lošim glasom.
          </p>
          <p
            style={{
              textAlign: "justify"
            }}
          >
            Al eto, karte su se drugačije izmješale pa sam uz puno truda postao
            fotograf.
          </p>
          <div style={{ textAlign: "center", marginTop: "5%" }}>
            <Link to="/kontakt">
              <Button
                type="primary"
                shape="round"
                size="large"
                style={{ width: "50%" }}
                onClick={this.updateClicks(1)}
              >
                Kontaktiraj me
              </Button>
            </Link>
          </div>
        </div>
        <div
          style={{
            width: "30vw",
            display: "flex",
            flexDirection: "column",
            justifyContent: "center"
          }}
        >
          <img
            src={gabelaPic}
            alt="slika_gabela"
            style={{ width: "30vw", height: "40vh" }}
          />
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  clientClicks: state.clicker.clicked
});

const mapDispatchToProps = {
  updateOnClick: clickerActions.updateOnClick
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Naslovna);
