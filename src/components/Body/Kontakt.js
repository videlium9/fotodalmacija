import React, { Component } from "react";
import GoogleMap from "./GoogleMap";

class Kontakt extends Component {
  render() {
    return (
      <div style={{}}>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            height: "100vh",
            alignItems: "center"
          }}
        >
          <div
            style={{
              width: "60vw",
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              textAlign: "center"
            }}
          >
            <h1
              style={{
                marginBottom: "30px",
                textAlign: "center"
              }}
            >
              Kontaktirajte me
            </h1>
            <p
              style={{
                textAlign: "center"
              }}
            >
              Adresa: Put Supavla 1, Split, Croatia
            </p>
            <p
              style={{
                textAlign: "center"
              }}
            >
              Mobilni: +385 (0)98 388932
            </p>
            <p
              style={{
                textAlign: "center"
              }}
            >
              E-mail: info@mirogabela.hr
            </p>
            <div style={{ textAlign: "center", marginTop: "5%" }} />
          </div>
          <div
            style={{
              width: "60vw",
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
              textAlign: "center"
            }}
          >
            <GoogleMap />
          </div>
        </div>
      </div>
    );
  }
}

export default Kontakt;
