const actionPrefix = "fotodalmacija/clicker/";

export const UPDATE_CLICKS = `${actionPrefix}UPDATE_CLICKS`;

const initialState = {
  clicked: 0
};

export const updateOnClick = newClickNum => dispatch =>
  dispatch({
    type: UPDATE_CLICKS,
    newClickNum
  });

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_CLICKS:
      return {
        ...state,
        clicked: action.newClickNum + 1
      };
    default:
      return state;
  }
};

export default reducer;
